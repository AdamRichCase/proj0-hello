# DESCRIPTION

This application returns a printed message as defined in the configuration file.


# AUTHOR

name: Adam Case

contact: acase@uoregon.edu
